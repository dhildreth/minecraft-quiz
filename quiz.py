quiz = [ 
    {
        'question': 'How do you survive an encounter with an enderman?',
        'answer': 'Run'
    },
    {
        'question': 'What is the most popular skin in Minecraft?',
        'answer': 'Steve'
    }
]

print("Welcome to the Minecraft Quiz!")
for question in quiz:
    print("Question:", question['question'])
    print("Answer:", question['answer'])
